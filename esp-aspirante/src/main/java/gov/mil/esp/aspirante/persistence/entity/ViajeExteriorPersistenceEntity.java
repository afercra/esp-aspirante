package gov.mil.esp.aspirante.persistence.entity;

import lombok.Data;

@Data
public class ViajeExteriorPersistenceEntity {
	
	String fecha;
	String paisVisitado;
	String motivo;
	String tiempoPermanencia;
	
}
