package gov.mil.esp.aspirante.domain.exceptions;

public class DiplomaNoExisteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public DiplomaNoExisteException(String message) {
		super(message);
	}

}
