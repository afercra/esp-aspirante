package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class AntecedenteJudicial implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
    String fecha;
    String tipoInvestigacion;
    String causa;
    String autoridad;
    String estadoActualDelProceso;
    String esReponsable;
}