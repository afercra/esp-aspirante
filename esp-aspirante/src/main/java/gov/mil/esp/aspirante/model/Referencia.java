package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Referencia implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	String primerApellido;
	String segundoApellido;
	String segundoNombre;
	String primerNombre;
	String nombreCompleto;
	String tipoDocumento;
	String numeroDocumento;
	String profesionUOficio;
	String direccion;
	String departamento;
	String ciudad;
	String numeroCelular;
	String numeroTelefono;
	String fuerza;
	String unidad;
	String cargo;
	String grado;
	String organismoEntidad;
	
}
