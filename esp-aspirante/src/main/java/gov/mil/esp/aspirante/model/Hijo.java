package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Hijo implements Serializable{

	private static final long serialVersionUID = 1L;
	
	String fechaNacimiento;
	String nombreCompleto;
	String numeroDocumento;
	String primerApellido;
	String primerNombre;
	String segundoNombre;
	String tipoDocumento;
	String edad;

}