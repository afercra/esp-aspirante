package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Empresa implements Serializable{

	private static final long serialVersionUID = 1L;
	
	String nombre;
	String direccion;
	String cargo;
	String tiempoEnEmpresa;
	String numeroCelular;
	String numeroTelefono;
	String sueldo;
	String motivoRetiro;
	String nombreJefe;
	String paginaWeb;
	String actividad;

}
