package gov.mil.esp.aspirante.persistence;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import gov.mil.esp.aspirante.persistence.entity.divipol.DivipolaPersistenceEntity;

public interface DivipolaRepository extends JpaRepository<DivipolaPersistenceEntity, Integer>{
	
	
	@Query(value = "SELECT * FROM espdb.divipola WHERE dvp_id = '0'", nativeQuery = true)
	List<DivipolaPersistenceEntity> getDepartamentos();
	
	@Query(value = "SELECT * FROM espdb.divipola WHERE dvp_id = :dvp_dep", nativeQuery = true)
	List<DivipolaPersistenceEntity> getDepartamentsCities(@Param("dvp_dep") String dvp_dep);

}
