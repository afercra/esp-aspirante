package gov.mil.esp.aspirante.model;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class Persona implements Serializable{
		
	private static final long serialVersionUID = 1L;
	
	@NotNull(message="El campo primer apellido no puede ser nulo.")
	@NotBlank(message = "El campo primer apellido no puede estar vacio.")
	private String primerApellido;
	
	private String segundoApellido;
	
	@NotNull(message="El campo primer Nombre es obligatorio.")
	@NotBlank(message = "El campo primer Nombre no puede estar vacio.")
	private String primerNombre;
	
	private String segundoNombre;
	
	@NotNull(message="El campo tipo documento de identidad es obligatorio.")
	@NotBlank(message = " El campo tipo documento de identidad no puede estar vacio ")
	private String tipoDocumentoIdentidad;
	
	@NotNull(message="El campo número documento de identidad es obligatorio.")
	@NotBlank(message = "El campo número documento de identidad no puede estar vacio")
	private String documentoIdentidad;
	
	private String numeroLibretaMilitar;
		
	private String claseLibretaMilitar;
		
	@NotNull(message="El campo pais nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo pais nacimiento no puede estar vacio.")
	private String paisNacimiento;
	
	@NotNull(message="El campo departamento nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo departamento nacimiento no puede estar vacio.")
	private String departamentoNacimiento;
	
	@NotNull(message="El campo ciudad nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo ciudad nacimiento no puede estar vacio.")
	private String ciudadNacimiento;
	
	
	@NotNull(message="El campo ciudad nacimiento no puede ser nulo.")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate fechaNacimiento;
		
	@NotNull(message="El campo estado civil no puede ser nulo.")
	@NotBlank(message = "El campo estado civil no puede estar vacio.")
	private String estadoCivil;
	
	@NotNull(message="El campo correo electronico no puede ser nulo.")
	@NotBlank(message = "El campo correo electronico no puede ser nulo.")
	private String correoElectronico;	
	
	private String division;
	
	private String brigada;
	
	private String unidadTactica;

}