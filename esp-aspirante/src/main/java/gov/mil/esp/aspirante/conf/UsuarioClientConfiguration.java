package gov.mil.esp.aspirante.conf;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Contract;

@Configuration
public class UsuarioClientConfiguration {
	
	@Bean
	public Contract useFeignAnnotations() {
		return new Contract.Default();
	}
}
