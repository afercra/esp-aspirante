package gov.mil.esp.aspirante.domain.exceptions;

public class UsuarioNoExisteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioNoExisteException(String message) {
		super(message);
	}
}
