package gov.mil.esp.aspirante.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import gov.mil.esp.aspirante.persistence.entity.PersonaPersistenceEntity;

public interface PersonaRepository extends JpaRepository<PersonaPersistenceEntity, String> {
	

	@Query(value=" UPDATE ESPDB.PROCESO SET FORMULARIO_DILIGENCIADO = true WHERE NUMERO_PROCESO = ( SELECT MAX (NUMERO_PROCESO) FROM ESPDB.PROCESO WHERE DOCUMENTO_IDENTIDAD = :documentoIdentidad )",nativeQuery = true)
	@Modifying
	void updateFormularioDiligenciado(@Param("documentoIdentidad") String documentoIdentidad);

}
