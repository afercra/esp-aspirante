package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ActividadEconomicaPrivada implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String razonSocial;
	private String actividad;

}