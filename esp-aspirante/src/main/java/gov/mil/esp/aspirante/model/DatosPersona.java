package gov.mil.esp.aspirante.model;

import java.io.Serializable;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class DatosPersona implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@NotNull(message = "El campo dependencia solicitante no puede ser nulo.")
	@NotBlank(message = "El campo dependencia solicitante no puede estar vacio.")
	private String dependenciaSolicitante;
	
	@NotNull(message = "El campo cargo cargo Aspirante  no puede ser nulo.")
	@NotBlank(message = "El campo cargo cargoAspirante Solicitante no puede estar vacio.")	
	private String cargoAspirante;

	@NotNull(message = "El campo primer apellido no puede ser nulo.")
	@NotBlank(message = "El campo primer apellido no puede estar vacio.")
	private String primerApellido;

	private String segundoApellido;

	private String grado;
	
	@NotNull(message = "El campo primer Nombre es obligatorio.")
	@NotBlank(message = "El campo primer Nombre no puede estar vacio.")
	private String primerNombre;

	private String segundoNombre;

	@NotNull(message = "El campo tipo documento de identidad es obligatorio.")
	@NotBlank(message = " El campo tipo documento de identidad no puede estar vacio ")
	private String tipoDocumentoIdentidad;

	@NotNull(message = "El campo número documento de identidad es obligatorio.")
	@NotBlank(message = "El campo número documento de identidad no puede estar vacio")
	private String documentoIdentidad;

	@NotNull(message = "El campo fecha expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo fecha expedición documento de identidad no puede estar vacio.")
	private String fechaExpedicionDocumentoIdentidad;

	@NotNull(message = "El campo departamento expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo departamento expedición documento de identidad no puede estar vacio.")
	private String departamentoExpedicionDocumentoIdentidad;

	@NotNull(message = "El campo ciudad expedición no puede ser nulo.")
	@NotBlank(message = "El campo ciudad expedición documento identidad no puede estar vacio.")
	private String ciudadExpedicionDocumentoIdentidad;

	@NotNull(message = "El campo pais expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo pais expedición documento de identidad no puede estar vacio.")
	private String paisExpedicionDocumentoIdentidad;

	private String numeroPasaporte;
	private String departamentoExpedicionPasaporte;
	private String ciudadExpedicionPasaporte;
	private String numeroLibretaMilitar;
	private String claseLibretaMilitar;
	private String fechaExpedicionLibretaMilitar;
	private String distritoLibretaMilitar;

	@NotNull(message = "El campo pais nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo pais nacimiento no puede estar vacio.")
	private String paisNacimiento;

	@NotNull(message = "El campo departamento nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo departamento nacimiento no puede estar vacio.")
	private String departamentoNacimiento;

	@NotNull(message = "El campo ciudad nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo ciudad nacimiento no puede estar vacio.")
	private String ciudadNacimiento;

	@NotNull(message = "El campo fecha de nacimiento no puede ser nulo.")
	@NotBlank(message = "El campo fecha de nacimiento no puede estar vacio.")
	private String fechaNacimiento;

	@NotNull(message = "El campo dirección residecia actual no puede ser nulo.")
	@NotBlank(message = "El campo dirección residecia actual de identidad no puede estar vacio.")
	private String direccionResidenciaActual;

	private String numeroCelularResidenciaActual;

	private String numeroTelefonoResidenciaActual;

	@NotNull(message = "El campo pais residencia actual no puede ser nulo.")
	@NotBlank(message = "El campo pais residencia actual no puede estar vacio.")
	private String paisResidenciaActual;

	@NotNull(message = "El campo pais expedición documento de identidad no puede ser nulo.")
	@NotBlank(message = "El campo pais expedición documento de identidad no puede estar vacio.")
	private String departamentoResidenciaActual;

	@NotNull(message = "El campo ciudad de residencia actual no puede ser nulo.")
	@NotBlank(message = "El campo ciudad de residencia actual no puede estar vacio.")
	private String ciudadResidenciaActual;

	@NotNull(message = "El campo barri	o de residencia actual no puede ser nulo.")
	@NotBlank(message = "El campo barrio de residencia actual no puede estar vacio.")
	private String barrioResidenciaActual;

	private String direccionResidenciaAnterior;

	private String numeroCelularResidenciaAnterior;

	private String numeroTelefonoResidenciaAnterior;
	private String paisResidenciaAnterior;
	private String departamentoResidenciaAnterior;
	private String ciudadResidenciaAnterior;

	private String barrioResidenciaAnterior;

	private String redesSociales;

	@NotNull(message = "El campo estado civil no puede ser nulo.")
	@NotBlank(message = "El campo estado civil no puede estar vacio.")
	private String estadoCivil;

	@NotNull(message = "El campo grupo sanguineo no puede ser nulo.")
	@NotBlank(message = "El campo  grupo sanguineo no puede estar vacio.")
	private String grupoSanguineo;

	private String profesion;

	private String tarjetaProfesional;

	@NotNull(message = "El campo estatura no puede ser nulo.")
	@NotBlank(message = "El campo estatura no puede estar vacio.")
	private String estatura;

	@NotNull(message = "El campo peso no puede ser nulo.")
	@NotBlank(message = "El campo peso no puede estar vacio.")
	private String peso;

	private String correoElectronico;

	private String primerApellidoCompaneroSentimental;
	private String segundoApellidoCompaneroSentimental;
	private String nombreCompletoCompaneroSentimental;
	private String primerNombreCompaneroSentimental;
	private String segundoNombreCompaneroSentimental;
	private String tipoDocumentoCompaneroSentimental;
	private String numeroDocumentoCompaneroSentimental;
	private String profesionUOficionCompaneroSentimental;
	private String direccionCompaneroSentimental;
	private String departamentoCompaneroSentimiental;
	private String ciudadCompaneroSentimental;
	private String numeroCelularCompaneroSentimental;
	private String numeroTelefonoCompaneroSentimental;
	private String correoElectronicoCompaneroSentimental;
	private String primerApellidoPadre;
	private String segundoApellidoPadre;
	private String primerNombrePadre;
	private String segundoNombrePadre;
	private String vivePadre;
	private String tipoDocumentoPadre;
	private String numeroDocumentoPadre;
	private String direccionPadre;
	private String departamentoPadre;
	private String ciudadPadre;
	private String numeroCelularPadre;
	private String numeroTelefonoPadre;
	private String ocupacionPadre;
	private String primerApellidoMadre;
	private String segundoApellidoMadre;
	private String primerNombreMadre;
	private String segundoNombreMadre;
	private String viveMadre;
	private String tipoDocumentoMadre;
	private String numeroDocumentoMadre;
	private String direccionMadre;
	private String departamentoMadre;
	private String ciudadMadre;
	private String numeroCelularMadre;
	private String numeroTelefonoMadre;
	private String ocupacionMadre;
	private String salarioIngresosLaborales;
	private String cesantiasIntereses;
	private String arriendos;
	private String honorarios;
	private String actividadesEnElexterior;
	private String armasQueConoce;
	private String fechaConocimientoArmas;
	private String armasQueHaManipualado;
	private String fechaYLugarManipulacionArmas;
	private String opinionSobreOrganizacionesIlegales;
	private String tieneInformacionActosCorrupcionEnInstitucion;
	private String informacionActosCorrupcionEnInstitucion;
	private String empleosugeridoPorPrimerApellido;
	private String empleosugeridoPorSegundoApellido;
	private String empleosugeridoPorPrimerNombre;
	private String empleosugeridoPorSegundoNombre;
	private String empleoSugeridoPorNombreCompleto;
	private String empleosugeridoPorDireccion;
	private String empleosugeridoPorTelefono;
	private String razonDeBuscarVinculoConlaInstitucionYExpectativas;
	private String datosAdicionales;
	private String redSocialConyugue;
	private String armasManipuladasEnCompaniaDe;

	private String nacionalidad;
	private String opinionGruposMarginales;
	private String conoceArmasDeFuegoExplosivosCuales;
	private String conoceArmasDeFuegoExplosivosCuandoYDonde;
	private String conoceArmasDeFuegoExplosivosCuando;
	private String conoceArmasDeFuegoExplosivosCualesHaManipulado;

	private String tieneInformacionRiesgosaParaNacion;
	private String informacionAmenazanteParaNacion;
	private String residenciaAnteriorDesde;
	private String residenciaAnteriorHasta;
	private String recomendadoPor;

	private List<Hijo> hijos;
	private List<Hermano> hermanos;
	private List<EstudioRealizado> estudiosRealizados;
	private List<Idioma> idiomas;
	private List<Empresa> empresas;
	private List<Referencia> referenciasNoFamiliares;
	private String esRecomendadoporAlguienDeLaInstitucion;
	private List<Referencia> parientesOAmigosEnLaInstitucion;
	private List<OtroIngreso> otrosIngresos;
	private List<Cuenta> cuentasDinero;
	private List<BienPatrimonial> bienesPatrimoniales;
	private List<ObligacionCrediticia> obligacionesCrediticias;
	private List<ParticipacionOrganizacion> participacionOrganizaciones;
	private List<ActividadEconomicaPrivada> actividadEconomicaPrivada;
	private List<ViajeExterior> viajesAlExterior;
	private List<OrganismoEstatal> organizacionesGubarnamentales;
	private List<AntecedenteJudicial> antecedentesJudiciales;
	private List<Referencia> contactoEmergencia;
	private List<Diploma> diplomas;
	
	private String otrosDatos;
	
	

	private String aspiranteFotoPerfilFrente;
	private byte [] aspiranteFotoPerfilFrenteBase64;
	
	private String aspiranteFotoDocumentoIdentidad;
	private byte [] aspiranteFotoDocumentoIdentidadBase64;
	
	private String aspiranteFotoCasaActual;
	private byte [] aspiranteFotoCasaActualBase64;
	
	
	
	
	
	

}