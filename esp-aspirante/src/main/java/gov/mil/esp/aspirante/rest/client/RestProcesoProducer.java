package gov.mil.esp.aspirante.rest.client;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import gov.mil.esp.aspirante.model.Persona;
import gov.mil.esp.aspirante.model.ProcesoDTO;
import gov.mil.esp.aspirante.rest.api.Request;
import gov.mil.esp.aspirante.rest.api.Response;

@Component
public class RestProcesoProducer {

	private RestTemplate restTemplate;

	@Value("${conf.esp.client.process.url}")
	private String procesoUrl;

	@PostConstruct
	public void init() {
		restTemplate = new RestTemplate();
	}

	public void enviarDocumentoAColaDeInicioProcesos(String numeroDocumento) {
		System.out.println(numeroDocumento);
		System.out.println(procesoUrl);

		ProcesoDTO procesoDTO = new ProcesoDTO();
		procesoDTO.setNumeroDocumento(numeroDocumento);

		Request<ProcesoDTO> request = new Request<ProcesoDTO>();
		request.setMensaje(procesoDTO);

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity httpRequest = new HttpEntity<>(request, headers);

		Response<Persona> response = restTemplate.postForObject(procesoUrl + "/proceso", httpRequest,
				Response.class);
		System.out.println("response is ------------->");
		System.out.println(response.getMessage());
		System.out.println(response.getPayload());

	}
}
