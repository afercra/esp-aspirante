package gov.mil.esp.aspirante.persistence.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "persona")
public class PersonaPersistenceEntity {
		
	@Id
	private String documentoIdentidad;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String segundoNombre;		
	private String tipoDocumentoIdentidad;
	
	private String numeroLibretaMilitar;
	private String claseLibretaMilitar;
	private String paisNacimiento;
	private String departamentoNacimiento;
	private String ciudadNacimiento;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private LocalDate fechaNacimiento;
	
	private String estadoCivil;
	private String correoElectronico;	
	private String division;
	private String brigada;
	private String batallon;
	
}