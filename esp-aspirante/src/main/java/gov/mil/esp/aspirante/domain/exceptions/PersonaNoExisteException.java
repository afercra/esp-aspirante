package gov.mil.esp.aspirante.domain.exceptions;

public class PersonaNoExisteException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PersonaNoExisteException(String message) {
		super(message);
	}

}
