package gov.mil.esp.aspirante.rest.client;

import java.net.ConnectException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import gov.mil.esp.aspirante.conf.UsuarioClientConfiguration;
import gov.mil.esp.aspirante.rest.api.Request;
import gov.mil.esp.aspirante.rest.api.Response;
import gov.mil.esp.aspirante.rest.api.Usuario;
import gov.mil.esp.aspirante.rest.client.fallback.UsuarioClientFallbackFactory;


@Component
@FeignClient(url="${conf.esp.client.user.url}", name="usuarioClient",configuration = UsuarioClientConfiguration.class, fallbackFactory = UsuarioClientFallbackFactory.class )
public interface UsuarioClient {
	
	@RequestLine("POST")
	@Headers("Content-Type: application/json")
	Response<Usuario> createUsuario(@RequestBody Request<Usuario> usuario) throws ConnectException;
	
	@RequestLine("GET /{usuario}")
	@Headers("Content-Type: application/json")
	Response<Usuario> getUsuario(@Param("usuario") String documentoIdentidad);

}
