package gov.mil.esp.aspirante;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.CrossOrigin;

@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@CrossOrigin(origins = "*")
public class EspAspiranteApplication {

	public static void main(String[] args) {
		SpringApplication.run(EspAspiranteApplication.class, args);
	}

}
