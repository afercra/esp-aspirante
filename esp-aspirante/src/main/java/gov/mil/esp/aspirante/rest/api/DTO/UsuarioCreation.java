package gov.mil.esp.aspirante.rest.api.DTO;

import java.io.Serializable;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import gov.mil.esp.aspirante.model.Persona;
import lombok.Data;

@Data
@JsonDeserialize
public class UsuarioCreation implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Valid
	private Persona persona;
	
	@NotNull(message = "El campo password debe estar presente.")
	@NotBlank(message = "El campo password debe tener un valor.")
	private String password;
	
	@NotNull(message = "El campo activo debe estar presente.")
	private boolean activo;
	@NotNull(message = "El campo tipoUsuario debe estar presente.")
	@NotBlank(message = "El campo tipoUsuario debe tener un valor.")
	private String tipoUsuario;
	
	@JsonCreator
	public UsuarioCreation(@JsonProperty Persona persona, String password, boolean activo, String tipoUsuario) {
		super();
		this.persona = persona;
		this.password = password;
		this.activo = activo;
		this.tipoUsuario = tipoUsuario;
	}
	
	

}
