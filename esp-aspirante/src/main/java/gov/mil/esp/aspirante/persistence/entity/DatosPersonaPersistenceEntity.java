package gov.mil.esp.aspirante.persistence.entity;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "datos_persona")
public class DatosPersonaPersistenceEntity {

	//private ObjectMapper objectMapper;

	@Id
	private String documentoIdentidad;
	
	//private String datosJSON;
	
	//@Convert(converter = HashMapConverter.class)

	private byte [] datos;
	
	private LocalDateTime fechaActualizacion;

	
	/*public void serializeCustomerAttributes() throws JsonProcessingException {
		this.datos = objectMapper.writeValueAsString(datosJSON);
	}
	
	public void deserializeCustomerAttributes() throws IOException {
	    this.datosJSON = objectMapper.readValue(datos, HashMap.class);
	}*/

}
