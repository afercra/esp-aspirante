package gov.mil.esp.aspirante.rest.api;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private String documentoIdentidad;
	private String password;
	private String rol;
	boolean activo;
	
/*
	@JsonCreator
	public Usuario(String documentoIdentidad, String password, String rol, boolean activo) {
		super();
		this.documentoIdentidad = documentoIdentidad;
		this.password = password;
		this.rol = rol;
		this.activo = activo;
	}
*/
	
	
}
