package gov.mil.esp.aspirante.domain.logic;

import java.net.ConnectException;
import java.util.List;

import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.jms.JmsException;

import gov.mil.esp.aspirante.domain.exceptions.DiplomaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.EnvioSolicitudInicioProcesoException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaSinDatosAsociadosException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioServiceException;
import gov.mil.esp.aspirante.model.DatosPersona;
import gov.mil.esp.aspirante.model.Diploma;
import gov.mil.esp.aspirante.model.Persona;
import gov.mil.esp.aspirante.rest.api.Usuario;
import gov.mil.esp.aspirante.rest.api.VerificacionDiploma;
import gov.mil.esp.aspirante.rest.api.DTO.UsuarioCreation;

public interface PersonaBusiness {

	public void saveDatosPersona(DatosPersona datosAspirante)
			throws DataAccessResourceFailureException, ConnectException, PersonaNoExisteException;

	DatosPersona getDatosPersona(String numeroDocumento);

	public DatosPersona getDatosPersonaPublic(String numeroDocumento);

	public Persona getPersona(String documentoIdentidad) throws PersonaNoExisteException;

	public UsuarioCreation createPersonaAndUser(UsuarioCreation usuarioCreation) throws ConnectException, JmsException, UsuarioServiceException, EnvioSolicitudInicioProcesoException;

	public UsuarioCreation getPersonaUsuario(String documentoIdentidad)
			throws PersonaNoExisteException, UsuarioNoExisteException;

	public UsuarioCreation getPersonaUsuarioList(String documentoIdentidad)
			throws PersonaNoExisteException, UsuarioNoExisteException;

	//public Persona saveAspirante(Persona persona);

	public Persona savePersona(Persona persona);

	public void changeFormularioDiligenciado(String documentoIdentidad);

	public void usuarioCreateRequestBuilder(Usuario usuario) throws  ConnectException, UsuarioServiceException;

	public List<Diploma> getDiplomas(String documentoIdentidad) throws PersonaSinDatosAsociadosException;

	public void saveVerificacionDiploma(VerificacionDiploma diploma, String documentoIdentidad)
			throws DataAccessResourceFailureException, ConnectException, DiplomaNoExisteException,
			PersonaSinDatosAsociadosException, PersonaNoExisteException;

}
