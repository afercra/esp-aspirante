package gov.mil.esp.aspirante.rest.api;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class VerificacionDiploma {

	@NotBlank(message = "El campo tituloObtenido no puede ser blanco")
	@NotNull(message = "Debe ingresar un Titulo para actualizar informacion")
	private String tituloObtenido;
	private byte[] certificadoInstitucion;
	private boolean esVeridico;
	private String observacion;
}
