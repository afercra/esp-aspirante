package gov.mil.esp.aspirante.domain.exceptions;

public class PersonaSinDatosAsociadosException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public PersonaSinDatosAsociadosException(String message) {
		super(message);
	}

}
