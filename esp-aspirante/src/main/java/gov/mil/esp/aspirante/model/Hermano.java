package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Hermano implements Serializable{
	
	private static final long serialVersionUID = 1L;

	String primerApellido;
	String segundoApellido;
	String primerNombre;
	String segundoNombre;
	String tipoDocumento;
	String numeroDocumento;
	String direccion;
	String departamento;
	String ciudad;
	String numeroCelular;
	String numeroTelefono;
	String ocupacion;
	String redesSociales;
	String nombreCompleto;
	String correoElectronico;

}