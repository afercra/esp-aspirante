package gov.mil.esp.aspirante.persistence.entity.divipol;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "divipol", schema = "espdb")
public class DivipolaPersistenceEntity{

	@Id
	@Column
	Integer id;
	
	@Column
	String nombre;
	
	@Column
	String codigo;
	
	@Column
	String dvp_id;
	
	
}
