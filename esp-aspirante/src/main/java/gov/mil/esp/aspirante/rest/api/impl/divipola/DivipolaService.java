package gov.mil.esp.aspirante.rest.api.impl.divipola;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import gov.mil.esp.aspirante.domain.logic.impl.DivipolaBusinessLogic;
import gov.mil.esp.aspirante.rest.api.DTO.divipol.Divipola;


@Component
public class DivipolaService {
	

	private DivipolaBusinessLogic divipolaBusinessLogic;
	
	public DivipolaService(DivipolaBusinessLogic divipolaBusinessLogic) {
		this.divipolaBusinessLogic = divipolaBusinessLogic;
	}
	
	public List<Divipola> getDepartamentos(){
		 
		List<Divipola> divipolaList =  new ArrayList<Divipola>();
		 
		divipolaBusinessLogic.getDepartamentos().forEach(x -> {
			 Divipola divipola = null;
			 divipola = new Divipola();
			 BeanUtils.copyProperties(x, divipola);
			 divipolaList.add(divipola);
		 });
		
		return divipolaList;
		
	}
	
	
	public List<Divipola> getDepartamentsCities(String id){
		 		
		List<Divipola> divipolaList =  new ArrayList<Divipola>();
		 
		divipolaBusinessLogic.getDepartamentsCities(id).forEach(x -> {
			 Divipola divipola = null;
			 divipola = new Divipola();
			 BeanUtils.copyProperties(x, divipola);
			 divipolaList.add(divipola);
		 });
		
		return divipolaList;
		
	}	
	
}
