package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class EstudioRealizado implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	String estudioRealizado;
	String anioFinalizacion;
	String tituloObtenido;
	String nombreInstitucion;
	String paisInstitucion;
	String departamentoInstitucion;
	String ciudad;

}
