package gov.mil.esp.aspirante.util.converter;

import java.util.Map;

import javax.persistence.AttributeConverter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public class HashMapConverter implements AttributeConverter<Map<String, Object>, String> {

	private ObjectMapper objectMapper;
	
	private final static Logger logger = LoggerFactory.getLogger(HashMapConverter.class);
	
	@Override
	public String convertToDatabaseColumn(Map<String, Object> datosPersona) {
		String datosPersonaJson = null;
		try {
			datosPersonaJson = objectMapper.writeValueAsString(datosPersona);
		} catch (Exception e) {
			logger.error("JSON writing error",e);
		}
		return datosPersonaJson;
	}

	@Override
	public Map<String, Object> convertToEntityAttribute(String datosPersonaDesdeJSON) {
		Map<String,Object> datosPersona = null;
		try {
			datosPersona = objectMapper.readValue(datosPersonaDesdeJSON, Map.class);
		} catch (Exception e) {
			logger.error("JSON reading error",e);
		}
		return datosPersona;
	}


	
}
