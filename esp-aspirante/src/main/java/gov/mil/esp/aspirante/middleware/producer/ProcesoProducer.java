package gov.mil.esp.aspirante.middleware.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProcesoProducer {
	
	private JmsTemplate jmsTemplate;
	
	@Autowired
	public ProcesoProducer(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	public void enviarIdentificacionAColaInicioProceso(String numeroDocumento) throws JmsException{
		System.out.println(numeroDocumento);
		jmsTemplate.convertAndSend("initProceso",numeroDocumento);
	}
	
	

}
