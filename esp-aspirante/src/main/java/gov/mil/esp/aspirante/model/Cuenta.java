package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class Cuenta implements Serializable {

	private static final long serialVersionUID = 1L;

	String pais;
	String entidadFinanciera;
	String sedeCuenta;
	String tipoCuenta;
	String numeroCuenta;
	String saldoCuenta;

}
