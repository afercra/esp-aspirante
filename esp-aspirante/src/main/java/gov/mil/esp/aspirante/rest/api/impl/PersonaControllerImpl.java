package gov.mil.esp.aspirante.rest.api.impl;

import java.net.ConnectException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.JmsException;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import gov.mil.esp.aspirante.domain.exceptions.DiplomaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.EnvioSolicitudInicioProcesoException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaSinDatosAsociadosException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioServiceException;
import gov.mil.esp.aspirante.model.DatosPersona;
import gov.mil.esp.aspirante.model.Persona;
import gov.mil.esp.aspirante.rest.api.Request;
import gov.mil.esp.aspirante.rest.api.Response;
import gov.mil.esp.aspirante.rest.api.VerificacionDiploma;
import gov.mil.esp.aspirante.rest.api.DTO.UsuarioCreation;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(value = "/persona")
public class PersonaControllerImpl {

	private PersonaService personaService;

	@Autowired
	public PersonaControllerImpl(PersonaService datosAspiranteService) {
		this.personaService = datosAspiranteService;
	}

	@PostMapping(value = "/datos")
	public ResponseEntity<Response<DatosPersona>> saveDatosPersona(
			@Valid @RequestBody Request<DatosPersona> datosAspirante) throws PersonaNoExisteException {
		Response<DatosPersona> response = null;

		try {
			response = personaService.saveDatosPersona(datosAspirante);
		} catch (DataAccessResourceFailureException e) {
			e.printStackTrace();
		} catch (ConnectException e) {
			e.printStackTrace();
		}
		return ResponseEntity.status(response.getHttpStatus()).body(response);
	}

	@PostMapping
	public ResponseEntity<Response<Persona>> savePersona(@Valid @RequestBody Request<Persona> persona)
			throws DataAccessResourceFailureException, ConnectException {
		Response<Persona> response = null;
		response = personaService.savePersona(persona);
		return ResponseEntity.status(response.getHttpStatus()).body(response);
	}

	@GetMapping(value = "/datos/{documentoIdentidad}")
	public ResponseEntity<Response<DatosPersona>> getDatosPersona(
			@PathVariable("documentoIdentidad") String documentoIdentidad) {
		Response<DatosPersona> response = null;
		response = personaService.getDatosPersona(documentoIdentidad);
		return ResponseEntity.status(response.getHttpStatus()).body(response);
	}

	@GetMapping("/{documentoIdentidad}")
	public ResponseEntity<Response<Persona>> getPersona(@PathVariable("documentoIdentidad") String documentoIdentidad) throws PersonaNoExisteException {
		Response<Persona> response = null;
		response = personaService.getPersona(documentoIdentidad);
		return ResponseEntity.status(response.getHttpStatus()).body(response);
	}

	@PostMapping(value = "/user")
	public ResponseEntity<?> createPersonaAndUser(@Valid @RequestBody Request<UsuarioCreation> request) throws ConnectException, JmsException, UsuarioServiceException, EnvioSolicitudInicioProcesoException {
		System.out.println(request);
		personaService.createPersonaAndUser(request);
		return ResponseEntity.status(HttpStatus.OK).body(null);
	}

	@GetMapping("/diploma/{documentoIdentidad}")
	public ResponseEntity<?> getDiplomas(@PathVariable("documentoIdentidad") String documentoIdentidad) throws PersonaSinDatosAsociadosException {
		return ResponseEntity.status(HttpStatus.OK).body(personaService.getDiplomas(documentoIdentidad));
	}

	@PutMapping("/diploma/{documentoIdentidad}")
	public ResponseEntity<?> saveVerificacionDiploma(@Valid @RequestBody Request<VerificacionDiploma> diploma,
			@PathVariable("documentoIdentidad") String documentoIdentidad) throws DataAccessResourceFailureException,
			ConnectException, DiplomaNoExisteException, PersonaSinDatosAsociadosException, PersonaNoExisteException {
		return ResponseEntity.status(HttpStatus.OK)
				.body(personaService.saveVerificacionDiploma(diploma, documentoIdentidad));
	}

	@GetMapping(value = "/user/{documentoIdentidad}")
	public ResponseEntity<?> getPersonaUsuario(@PathVariable("documentoIdentidad") String documentoIdentidad)
			throws PersonaNoExisteException, UsuarioNoExisteException {
		return ResponseEntity.status(HttpStatus.OK).body(personaService.getPersonaUsuario(documentoIdentidad));
	}

}
