package gov.mil.esp.aspirante.domain.logic.impl;

import java.net.ConnectException;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.jms.JmsException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.SerializationUtils;

import gov.mil.esp.aspirante.domain.exceptions.DiplomaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.EnvioSolicitudInicioProcesoException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaSinDatosAsociadosException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioServiceException;
import gov.mil.esp.aspirante.domain.logic.PersonaBusiness;
import gov.mil.esp.aspirante.middleware.producer.ProcesoProducer;
import gov.mil.esp.aspirante.model.DatosPersona;
import gov.mil.esp.aspirante.model.Diploma;
import gov.mil.esp.aspirante.model.Persona;
import gov.mil.esp.aspirante.persistence.DatosPersonaRepository;
import gov.mil.esp.aspirante.persistence.PersonaRepository;
import gov.mil.esp.aspirante.persistence.entity.DatosPersonaPersistenceEntity;
import gov.mil.esp.aspirante.persistence.entity.PersonaPersistenceEntity;
import gov.mil.esp.aspirante.rest.api.Request;
import gov.mil.esp.aspirante.rest.api.Response;
import gov.mil.esp.aspirante.rest.api.Usuario;
import gov.mil.esp.aspirante.rest.api.VerificacionDiploma;
import gov.mil.esp.aspirante.rest.api.DTO.UsuarioCreation;
import gov.mil.esp.aspirante.rest.client.UsuarioClient;

@Component
public class PersonaBusinessImpl implements PersonaBusiness {

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonaBusinessImpl.class);

	private DatosPersonaRepository datosAspiranteRepository;
	private PersonaRepository personaRepository;
	private UsuarioClient usuarioClient;
	private ProcesoProducer procesoProducer;

	@Autowired
	public PersonaBusinessImpl(DatosPersonaRepository datosAspiranteRepository, PersonaRepository personaRepository,
			UsuarioClient usuarioClient, ProcesoProducer procesoProducer) {
		this.datosAspiranteRepository = datosAspiranteRepository;
		this.personaRepository = personaRepository;
		this.usuarioClient = usuarioClient;
		this.procesoProducer = procesoProducer;
	}

	@Transactional
	public void saveDatosPersona(DatosPersona datosAspirante)
			throws DataAccessResourceFailureException, ConnectException, PersonaNoExisteException {

		getPersona(datosAspirante.getDocumentoIdentidad());

		DatosPersonaPersistenceEntity datosPersonaPersistenceEntity = new DatosPersonaPersistenceEntity();
		datosPersonaPersistenceEntity.setDocumentoIdentidad(datosAspirante.getDocumentoIdentidad());
		datosPersonaPersistenceEntity.setDatos(SerializationUtils.serialize(datosAspirante));
		datosAspiranteRepository.saveAndFlush(datosPersonaPersistenceEntity);
		this.changeFormularioDiligenciado(datosAspirante.getDocumentoIdentidad());
	}

	public DatosPersona getDatosPersona(String numeroDocumento) {

		DatosPersona datosPersona = new DatosPersona();
		DatosPersonaPersistenceEntity datosAspirantePersistenceEntity;
		Optional<DatosPersonaPersistenceEntity> datosAspirantePersistenceEntityOptional;
		datosAspirantePersistenceEntityOptional = datosAspiranteRepository.findById(numeroDocumento);
		datosAspirantePersistenceEntity = datosAspirantePersistenceEntityOptional
				.orElse(new DatosPersonaPersistenceEntity());

		byte[] datosPersonaBytes = null;

		datosPersonaBytes = datosAspirantePersistenceEntity.getDatos();
		Object object = SerializationUtils.deserialize(datosPersonaBytes);
		datosPersona = (DatosPersona) object;
		return datosPersona;
	}

	public DatosPersona getDatosPersonaPublic(String numeroDocumento) {
		DatosPersona datosAspirante = this.getDatosPersona(numeroDocumento);
		return datosAspirante;
	}

	public Persona getPersona(String documentoIdentidad) throws PersonaNoExisteException {
		Persona persona = new Persona();
		PersonaPersistenceEntity personaPersistenceEntity = new PersonaPersistenceEntity();

		Optional<PersonaPersistenceEntity> personaOptional = personaRepository.findById(documentoIdentidad);
		personaPersistenceEntity = personaOptional.orElseThrow(() -> new PersonaNoExisteException(
				"La persona con el documento de Identidad: " + documentoIdentidad + " no existe"));

		BeanUtils.copyProperties(personaPersistenceEntity, persona);
		persona.setUnidadTactica(personaPersistenceEntity.getBatallon());
		return persona;
	}

	public UsuarioCreation createPersonaAndUser(UsuarioCreation usuarioCreation)
			throws ConnectException, JmsException, UsuarioServiceException, EnvioSolicitudInicioProcesoException {

		String tipoUsuario = usuarioCreation.getTipoUsuario();
		Persona persona = usuarioCreation.getPersona();

		savePersona(persona);

		usuarioCreateRequestBuilder(
				Usuario.builder().documentoIdentidad(usuarioCreation.getPersona().getDocumentoIdentidad())
						.password(usuarioCreation.getPassword()).activo(usuarioCreation.isActivo())
						.rol(usuarioCreation.getTipoUsuario()).build());

		if (tipoUsuario.equals("Aspirante")) {
			try {
				procesoProducer.enviarIdentificacionAColaInicioProceso(persona.getDocumentoIdentidad());
			} catch (JmsException e) {
				throw new EnvioSolicitudInicioProcesoException("Se creo el usuario y persona para el documento: "
						+ persona.getDocumentoIdentidad() + ", pero El servicio de procesos no esta disponible, "
						+ " por tanto no se pudo enviar la solicitud de inicio de proceso, intente esta peticion de nuevo o intente iniciar el proceso manualmente o.");
			}

		}
		return usuarioCreation;

	}

	public UsuarioCreation getPersonaUsuario(String documentoIdentidad)
			throws PersonaNoExisteException, UsuarioNoExisteException {

		PersonaPersistenceEntity personaPersistenceEntity = personaRepository.findById(documentoIdentidad).orElseThrow(
				() -> new PersonaNoExisteException("No existe una persona con documento: " + documentoIdentidad));

		Persona persona = new Persona();

		BeanUtils.copyProperties(personaPersistenceEntity, persona);

		Response<Usuario> usuarioResponse = usuarioClient.getUsuario(documentoIdentidad);

		Usuario usuario = usuarioResponse.getPayload();

		if (usuario == null) {
			throw new UsuarioNoExisteException(
					"No existe usuario registrado para el documento de identidad: " + documentoIdentidad);
		}

		UsuarioCreation usuarioCreation = new UsuarioCreation(persona, usuario.getPassword(), usuario.isActivo(),
				usuario.getRol());

		return usuarioCreation;
	}

	public UsuarioCreation getPersonaUsuarioList(String documentoIdentidad)
			throws PersonaNoExisteException, UsuarioNoExisteException {

		PersonaPersistenceEntity personaPersistenceEntity = personaRepository.findById(documentoIdentidad).orElseThrow(
				() -> new PersonaNoExisteException("No existe una persona con documento: " + documentoIdentidad));

		Persona persona = new Persona();

		BeanUtils.copyProperties(personaPersistenceEntity, persona);

		Response<Usuario> usuarioResponse = usuarioClient.getUsuario(documentoIdentidad);

		Usuario usuario = usuarioResponse.getPayload();

		if (usuario == null) {
			throw new UsuarioNoExisteException(
					"No existe usuario registrado para el documento de identidad: " + documentoIdentidad);
		}

		UsuarioCreation usuarioCreation = new UsuarioCreation(persona, usuario.getPassword(), usuario.isActivo(),
				usuario.getRol());

		return usuarioCreation;
	}

	/*public Persona saveAspirante(Persona persona) {

		savePersona(persona);
		procesoProducer.enviarIdentificacionAColaInicioProceso(persona.getDocumentoIdentidad());
		return persona;
	}*/

	@Transactional(propagation = Propagation.REQUIRES_NEW )
	public Persona savePersona(Persona persona) {

		PersonaPersistenceEntity personaPersistenceEntity = new PersonaPersistenceEntity();
		BeanUtils.copyProperties(persona, personaPersistenceEntity);

		personaPersistenceEntity.setBatallon(persona.getUnidadTactica());

		personaRepository.saveAndFlush(personaPersistenceEntity);
	//	personaRepository.flush();
		return persona;
	}

	public void changeFormularioDiligenciado(String documentoIdentidad) {
		personaRepository.updateFormularioDiligenciado(documentoIdentidad);
	}
	
	//@Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.READ_COMMITTED)
	public void usuarioCreateRequestBuilder(Usuario usuario) throws ConnectException, UsuarioServiceException {
		Request<Usuario> usuarioRequest = new Request<Usuario>();

		usuarioRequest.setMensaje(usuario);
		System.out.println("usuario request");
		System.out.println(usuarioRequest);

		System.out.println("into usuariocreaterequestbuilder -------------__>");
		Response<Usuario> usuarioResponse = null;

		try {
			usuarioResponse = usuarioClient.createUsuario(usuarioRequest);
		} catch (ConnectException e) {
			LOGGER.error("Error comunicandose con el servidor, al intentar crear el usuario : {}", usuarioRequest, e);
			throw new ConnectException(
					"No se pudo crear El usuario, el servicio de usuarios no se encuentra disponible");
		} catch (Exception e) {
			LOGGER.error("Error comunicandose con el servidor, al intentar crear el usuario : {}", usuarioRequest, e);
			throw new UsuarioServiceException(
					"No se pudo crear El usuario, el servicio de usuarios no se encuentra disponible");
		}

		System.out.println("after usuario request  ---------------------_>");
		System.out.println(usuarioResponse);

	}

	public List<Diploma> getDiplomas(String documentoIdentidad) throws PersonaSinDatosAsociadosException {
		DatosPersona datosPersona = getDatosPersona(documentoIdentidad);
		personaRepository.findById(documentoIdentidad)
				.orElseThrow(() -> new PersonaSinDatosAsociadosException("La persona con el documento de identidad: "+documentoIdentidad+" no existe o no tiene datos asociados"));

		return datosPersona.getDiplomas();
	}

	@Transactional
	public void saveVerificacionDiploma(VerificacionDiploma diploma, String documentoIdentidad)
			throws DataAccessResourceFailureException, ConnectException, DiplomaNoExisteException,
			PersonaSinDatosAsociadosException, PersonaNoExisteException {

		DatosPersona datosPersona = getDatosPersona(documentoIdentidad);

		if (datosPersona == null) {
			throw new PersonaSinDatosAsociadosException("La Persona No tiene datos Asociados ");
		}

		List<Diploma> diplomas = datosPersona.getDiplomas();

		int coincidenciasNombreDiploma = 0;

		for (int i = 0; i < diplomas.size(); i++) {
			
			Diploma diplomaArreglo = diplomas.get(i);
			
			if (diplomaArreglo.getTituloObtenido().equals(diploma.getTituloObtenido())) {

				diplomaArreglo.setCertificadoInstitucion(diploma.getCertificadoInstitucion());
				diplomaArreglo.setEsVeridico(diploma.isEsVeridico());
				diplomaArreglo.setObservacion(diploma.getObservacion());
				diplomas.set(i, diplomaArreglo);

				coincidenciasNombreDiploma++;
			}
		}

		if (coincidenciasNombreDiploma == 0) {
			throw new DiplomaNoExisteException(
					"El diploma: " + diploma.getTituloObtenido() + " no existe para el usuario: " + documentoIdentidad);
		}

		datosPersona.setDiplomas(diplomas);

		saveDatosPersona(datosPersona);

	}

}
