package gov.mil.esp.aspirante.domain.logic.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import gov.mil.esp.aspirante.model.divipola.Divipola;
import gov.mil.esp.aspirante.persistence.DivipolaRepository;

@Component
public class DivipolaBusinessLogic {

	private DivipolaRepository divipolaRepository;

	
	@Autowired
	public DivipolaBusinessLogic (DivipolaRepository divipolaRepository) {
		this.divipolaRepository = divipolaRepository;
	}
	
	public List<Divipola> getDepartamentos(){
		 
		List<Divipola> divipolaList =  new ArrayList<Divipola>();
		 
		 divipolaRepository.getDepartamentos().forEach(x -> {
			 Divipola divipola = null;
			 divipola = new Divipola();
			 BeanUtils.copyProperties(x, divipola);
			 divipolaList.add(divipola);
		 });
		
		return divipolaList;
		
	}
	
	public List<Divipola> getDepartamentsCities(String Dvp_id){
		 
		List<Divipola> divipolaList =  new ArrayList<Divipola>();
		 
		 divipolaRepository.getDepartamentsCities(Dvp_id).forEach(x -> {
			 Divipola divipola = null;
			 divipola = new Divipola();
			 BeanUtils.copyProperties(x, divipola);
			 divipolaList.add(divipola);
		 });
		
		return divipolaList;
		
	}	
}
