package gov.mil.esp.aspirante.domain.exceptions;

public class EnvioSolicitudInicioProcesoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EnvioSolicitudInicioProcesoException(String message) {
		super(message);
	}

}
