package gov.mil.esp.aspirante.rest.client.fallback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import gov.mil.esp.aspirante.rest.client.UsuarioClient;

@Component
public class UsuarioClientFallbackFactory implements FallbackFactory<UsuarioClient>{

	private final static Logger LOGGER = LoggerFactory.getLogger(UsuarioClientFallbackFactory.class);
	
	@Override
	public UsuarioClient create(Throwable cause) {
		System.out.println("asddddddddddddddddddddddddddddd fallabcka factory");
		LOGGER.error("ON fallbakc factory", cause);
		return null;
	}

}
