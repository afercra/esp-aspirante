package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Diploma implements Serializable {
	 
	private static final long serialVersionUID = 1L;
	 
	 private String tituloObtenido;
	 private String diplomaPDFPath;
	 private byte [] diplomaPDF64;
	 private byte [] certificadoInstitucion;
	 private boolean esVeridico;
	 private String observacion;
}
