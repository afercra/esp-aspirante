package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ViajeExterior implements Serializable {

	private static final long serialVersionUID = 1L;

	String fecha;
	String paisVisitado;
	String motivo;
	String tiempoPermanencia;
}
