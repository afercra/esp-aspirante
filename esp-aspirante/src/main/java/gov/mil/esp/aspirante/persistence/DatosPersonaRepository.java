package gov.mil.esp.aspirante.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import gov.mil.esp.aspirante.persistence.entity.DatosPersonaPersistenceEntity;

public interface DatosPersonaRepository extends JpaRepository<DatosPersonaPersistenceEntity, String> {

	
}
