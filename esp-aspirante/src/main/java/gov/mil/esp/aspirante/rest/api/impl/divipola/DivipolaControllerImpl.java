package gov.mil.esp.aspirante.rest.api.impl.divipola;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class DivipolaControllerImpl {

	
	private DivipolaService divipolaService;
	
	public DivipolaControllerImpl(DivipolaService divipolaService) {
		this.divipolaService = divipolaService;
	}
	
	@GetMapping(value="/departamento")
	public ResponseEntity<?> getDepartamento() {		
		return ResponseEntity.status(HttpStatus.OK).body(divipolaService.getDepartamentos());
	}
	
	@GetMapping(value="/ciudad/{codigoDepartamento}")
	public ResponseEntity<?> getDepartamento( @PathVariable String codigoDepartamento) {		
		return ResponseEntity.status(HttpStatus.OK).body(divipolaService.getDepartamentsCities(codigoDepartamento));
	} 
	
}
