package gov.mil.esp.aspirante.model;

import lombok.Data;

@Data
public class ProcesoDTO {
	
	String numeroDocumento;
}