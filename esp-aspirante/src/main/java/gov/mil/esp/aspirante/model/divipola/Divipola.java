package gov.mil.esp.aspirante.model.divipola;

import lombok.Data;

@Data

public class Divipola{

	Integer id;
	String nombre;
	String codigo;
	String dvp_id;
	
	
}
