package gov.mil.esp.aspirante.domain.exceptions;

public class UsuarioServiceException extends Exception {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public UsuarioServiceException (String message) {
		super(message);
	}
}
