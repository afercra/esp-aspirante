package gov.mil.esp.aspirante.rest.api.impl;

import java.net.ConnectException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.jms.JmsException;
import org.springframework.stereotype.Service;

import gov.mil.esp.aspirante.domain.exceptions.DiplomaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.EnvioSolicitudInicioProcesoException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.PersonaSinDatosAsociadosException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioServiceException;
import gov.mil.esp.aspirante.domain.logic.impl.PersonaBusinessImpl;
import gov.mil.esp.aspirante.model.DatosPersona;
import gov.mil.esp.aspirante.model.Diploma;
import gov.mil.esp.aspirante.model.Persona;
import gov.mil.esp.aspirante.rest.api.Request;
import gov.mil.esp.aspirante.rest.api.Response;
import gov.mil.esp.aspirante.rest.api.VerificacionDiploma;
import gov.mil.esp.aspirante.rest.api.DTO.UsuarioCreation;
import gov.mil.esp.aspirante.rest.client.RestProcesoProducer;

@Service
public class PersonaService {

	private PersonaBusinessImpl datosPersonaLogic;
	private RestProcesoProducer procesoConsumer;

	@Autowired
	public PersonaService(PersonaBusinessImpl datosAspiranteLogic, RestProcesoProducer procesoConsumer) {
		this.datosPersonaLogic = datosAspiranteLogic;
		this.procesoConsumer = procesoConsumer;
	}

	private Response<DatosPersona> response;

	private Response<Persona> responsePersona;

	public Response<Persona> savePersona(Request<Persona> request) {
		datosPersonaLogic.savePersona(request.getMensaje());
		buildPersonaResponseMessage(HttpStatus.OK, "200", "Se creo la persona con exito", null);
		return responsePersona;
	}

	public Response<DatosPersona> saveDatosPersona(Request<DatosPersona> request)
			throws DataAccessResourceFailureException, ConnectException, PersonaNoExisteException {
		datosPersonaLogic.saveDatosPersona(request.getMensaje());
		buildResponseMessage(HttpStatus.OK, "200", "Formulario almacenado con exito", null);
		// procesoConsumer.enviarDocumentoAColaDeInicioProcesos(request.getMensaje().getDocumentoIdentidad());
		return response;

	}

	public Response<DatosPersona> getDatosPersona(String documentoIdentidad) {
		buildResponseMessage(HttpStatus.OK, "200", "Los datos del aspirante son: ",
				datosPersonaLogic.getDatosPersonaPublic(documentoIdentidad));
		return (Response<DatosPersona>) response;
	}

	public Response<Persona> getPersona(String documentoIdentidad) throws PersonaNoExisteException {
		buildPersonaResponseMessage(HttpStatus.OK, "200", "Los datos del aspirante son: ",
				datosPersonaLogic.getPersona(documentoIdentidad));
		return (Response<Persona>) responsePersona;
	}

	public Response<?> createPersonaAndUser(Request<UsuarioCreation> request) throws ConnectException, JmsException, UsuarioServiceException, EnvioSolicitudInicioProcesoException {
		UsuarioCreation usuarioCreation = request.getMensaje();
		datosPersonaLogic.createPersonaAndUser(usuarioCreation);

		Response response = new Response<UsuarioCreation>();
		response.setHttpStatus(HttpStatus.OK);
		response.setPayload(request.getMensaje());
		response.setMessage("Aspirante creado exitosamente: ");
		return response;

	}
	
	
	public Response<?> getPersonaUsuario(String documentoIdentidad) throws PersonaNoExisteException, UsuarioNoExisteException {
		
		Response response = new Response<UsuarioCreation> ();
		response.setHttpStatus(HttpStatus.OK);
		response.setMessage("El usuario persona es: ");
		response.setPayload(datosPersonaLogic.getPersonaUsuario(documentoIdentidad));
		return response;
	}
	
	public Response<?> getPersonaUsuarioList(String documentoIdentidad) throws PersonaNoExisteException, UsuarioNoExisteException {
		
		Response response = new Response<UsuarioCreation> ();
		response.setHttpStatus(HttpStatus.OK);
		response.setMessage("El usuario persona es: ");
		response.setPayload(datosPersonaLogic.getPersonaUsuarioList(documentoIdentidad));
		return response;
	}

	public Response<?> getDiplomas(String documentoIdentidad) throws PersonaSinDatosAsociadosException {
		List<Diploma> diplomasList = datosPersonaLogic.getDiplomas(documentoIdentidad);
		Response response = new Response<Diploma>();
		response.setHttpStatus(HttpStatus.OK);
		response.setMessage("La lista de diplomas es: ");
		response.setPayload(diplomasList);
		return response;
	}
	
	public Response<?> saveVerificacionDiploma(Request<VerificacionDiploma> diploma, String documentoIdentidad)
			throws DataAccessResourceFailureException, ConnectException, DiplomaNoExisteException,
			PersonaSinDatosAsociadosException, PersonaNoExisteException {
		datosPersonaLogic.saveVerificacionDiploma( diploma.getMensaje(), documentoIdentidad);
		Response response = new Response<Diploma>();
		response.setHttpStatus(HttpStatus.OK);
		response.setMessage("Diploma Almacenado Con exito");
		return response;
	}

	private void buildResponseMessage(HttpStatus httpStatus, String codigo, String mensaje, DatosPersona payload) {
		response = new Response<DatosPersona>();
		response.setHttpStatus(httpStatus);
		response.setCode(codigo);
		response.setMessage(mensaje);
		response.setPayload(payload);

	}

	private void buildPersonaResponseMessage(HttpStatus httpStatus, String codigo, String mensaje, Persona payload) {
		responsePersona = new Response<Persona>();
		responsePersona.setHttpStatus(httpStatus);
		responsePersona.setCode(codigo);
		responsePersona.setMessage(mensaje);
		responsePersona.setPayload(payload);
	}
}