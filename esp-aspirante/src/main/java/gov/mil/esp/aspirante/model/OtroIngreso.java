package gov.mil.esp.aspirante.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class OtroIngreso implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	String concepto;
	String valor;

}
