package gov.mil.esp.aspirante.rest.api;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Response<T> {

	private HttpStatus httpStatus;
	private String code;
	private Object message;
	private T payload;
	private LocalDateTime timestamp;

}
