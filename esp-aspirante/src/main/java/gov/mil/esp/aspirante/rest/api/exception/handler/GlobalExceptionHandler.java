package gov.mil.esp.aspirante.rest.api.exception.handler;

import java.net.ConnectException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import feign.FeignException.BadRequest;
import gov.mil.esp.aspirante.domain.exceptions.DiplomaNoExisteException;
import gov.mil.esp.aspirante.domain.exceptions.EnvioSolicitudInicioProcesoException;
import gov.mil.esp.aspirante.domain.exceptions.UsuarioNoExisteException;
import gov.mil.esp.aspirante.rest.api.Response;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private Response<?> response;

	// Handling for the mandatory fields constraint
	@Override
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		// Get all fields errors
		System.out.println("badRequest");
		System.out.println(ex);
		ex.getStackTrace();
		List<String> errors = ex.getBindingResult().getFieldErrors().stream().map(x -> x.getDefaultMessage())
				.collect(Collectors.toList());
		StringBuilder errorsString = new StringBuilder();

		for (String string : errors) {
			errorsString.append(string + " ");
		}
		createResponse(Integer.toString(status.value()), errorsString.toString());
		return ResponseEntity.status(status).body(response);
			
	}

	@ExceptionHandler(Exception.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Response> handleGeneralError(Exception ex) {
		System.out.println("general exception");
		ex.printStackTrace();
		// cuando el cliente se intenta conectar con procesos y
		createResponse(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()),
				"ocurrio un error interno: " + ex.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}
	

	@ExceptionHandler(BadRequest.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Response> handle(BadRequest ex) {
		System.out.println("DataAccessResourceFailureException");
		createResponse(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()), "ocurrio un error interno");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}

	@ExceptionHandler(DataAccessResourceFailureException.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Response> handle(DataAccessResourceFailureException ex) {
		System.out.println("DataAccessResourceFailureException");
		createResponse(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()), "ocurrio un error interno");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}

	@ExceptionHandler(ConnectException.class)
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public ResponseEntity<Response> handle(ConnectException ex) {
		System.out.println("connect exception");
		ex.getStackTrace();
		// cuando el cliente se intenta conectar con procesos y
		createResponse(Integer.toString(HttpStatus.INTERNAL_SERVER_ERROR.value()), "ocurrio un error interno");
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
	}

	@ExceptionHandler(DiplomaNoExisteException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public ResponseEntity<Response> handle(DiplomaNoExisteException ex) {
		ex.getStackTrace();
		// cuando el cliente se intenta conectar con procesos y
		createResponse(Integer.toString(HttpStatus.NOT_FOUND.value()), "El diploma no existe ");
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}

	@ExceptionHandler(UsuarioNoExisteException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public ResponseEntity<Response> handle(UsuarioNoExisteException ex) {
		ex.printStackTrace();
		// cuando el cliente se intenta conectar con procesos y
		createResponse(Integer.toString(HttpStatus.NOT_FOUND.value()), ex.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
	}


	@ExceptionHandler(EnvioSolicitudInicioProcesoException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public ResponseEntity<Response> handle(EnvioSolicitudInicioProcesoException ex) {
		ex.printStackTrace();
		// cuando el cliente se intenta conectar con procesos y
		createResponse(Integer.toString(HttpStatus.SERVICE_UNAVAILABLE.value()), ex.getMessage());
		return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(response);
	}
	


	private Response<?> createResponse(String code, String message) {
		response = new Response<String>();
		response.setCode(code);
		response.setMessage(message);
		response.setTimestamp(LocalDateTime.now());
		return response;
	}
}
